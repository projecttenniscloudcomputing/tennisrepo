#include "..\include\ScoresRequest.h"

#include<iostream>
ScoresRequest::ScoresRequest():Request("Scores")
{
	int choice;
	std::string request;

	std::cout << "Please choose one of the following options:" << std::endl;

	std::cout << " 1 - To show all scores from a certain year." << std::endl;
	std::cout << " 2 - To show all scores from a certain player." << std::endl;

	std::cin >> choice;

	int year;
	std::string playerId;

	switch (choice)
	{
	case 1:
	{
		std::cout << "Please enter the year you want to see scores from." << std::endl;
		std::cin >> year;
		this->content.push_back(boost::property_tree::ptree::value_type("Choice", std::to_string(choice)));
		this->content.push_back(boost::property_tree::ptree::value_type("Year", std::to_string(year)));

		break;
	}
	case 2:
	{
		std::cout << "Please insert the id of the player you want to see scores from." << std::endl;
		std::cin >> playerId;
		this->content.push_back(boost::property_tree::ptree::value_type("Choice", std::to_string(choice)));
		this->content.push_back(boost::property_tree::ptree::value_type("PlayerId", playerId));
		break;
	}
	}

	
}
