#include "RequestsManager.h"

#include"BestWorstPlayerRequest.h"
#include"PlayersRankingRequest.h"
#include"ScoresRequest.h"
#include"PlayerAfterCriteriaRequest.h"

#include<iostream>
RequestsManager::RequestsManager()
{

}

void RequestsManager::emplaceBestWorstPlayerRequest()
{
	this->requests.emplace("BestWorstPlayer", std::make_shared<BestWorstPlayerRequest>());
}

void RequestsManager::emplacePlayerAfterCriteriaRequest()
{
	this->requests.emplace("PlayerAfterCriteria", std::make_shared<PlayerAfterCriteriaRequest>());
}

void RequestsManager::emplacePlayersRankingRequest()
{
	this->requests.emplace("PlayersRanking", std::make_shared<PlayersRankingRequest>());
}

void RequestsManager::emplaceScoreRequest()
{
	this->requests.emplace("Scores", std::make_shared<ScoresRequest>());
}

std::map<std::string, std::shared_ptr<Framework::Request>> RequestsManager::getMap() const
{
	return this->requests;
}
