#include "..\include\PlayersRankingRequest.h"

#include<iostream>
PlayersRankingRequest::PlayersRankingRequest():Request("PlayersRanking")
{
	int choice;
	std::string request;

	std::cout << "Please choose one of the following options:" << std::endl;

	std::cout << " 1 - To show the top 10 players from a certain country from the player list." << std::endl;
	std::cout << " 2 - To show the top 10 players from a certain year from the player list." << std::endl;

	std::cin >> choice;

	std::string flagCode;
	int year;

	switch (choice)
	{
	case 1:
	{
		std::cout << "Please the flag code of the players." << std::endl;
		std::cin >> flagCode;
		this->content.push_back(boost::property_tree::ptree::value_type("Choice", std::to_string(choice)));
		this->content.push_back(boost::property_tree::ptree::value_type("FlagCode", flagCode));

		break;
	}
	case 2:
	{
		std::cout << "Please insert the year when the players played." << std::endl;
		std::cin >> year;
		this->content.push_back(boost::property_tree::ptree::value_type("Choice", std::to_string(choice)));
		this->content.push_back(boost::property_tree::ptree::value_type("Year", std::to_string(year)));
		break;
	}
	}


	
}
