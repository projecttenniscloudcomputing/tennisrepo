#include<iostream>

#include "..\include\BestWorstPlayerRequest.h"

BestWorstPlayerRequest::BestWorstPlayerRequest():Request("BestWorstPlayer")
{
	int choice;
	std::string request;

	std::cout << "Please choose one of the following options :" << std::endl;

	std::cout << "1 - To show the player/players with the best score from the player list." << std::endl;
	std::cout << "2 - To show the player/players with the worst score from player list." << std::endl;

	std::cin >> choice;

	request = std::to_string(choice);

	this->content.push_back(boost::property_tree::ptree::value_type("Request", request));
}
