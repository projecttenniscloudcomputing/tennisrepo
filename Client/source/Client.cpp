#include <iostream>
#include <memory>

#include "Session.h"

int main(int argc, char** argv)
{
	const auto host = "127.0.0.1";
	const auto port = "2751";

	// The io_context is required for all I/O
	boost::asio::io_context ioContext;

	RequestsManager requestsManager;

	int choice;

	std::cout << "Please choose one option from below:" << std::endl;

	std::cout << "1 - To get the best/worst player from the player list." << std::endl;
	std::cout << "2 - To get the top players from a certain country/year." << std::endl;
	std::cout << "3 - To get all scores of matches from different year/player." << std::endl;
	std::cout << "4 - To get all players from with a certain first name , last name or tour name." << std::endl;

	std::cin >> choice;

	switch (choice)
	{
	case 1:
	{
		requestsManager.emplaceBestWorstPlayerRequest();
		auto bestWorstPlayer = requestsManager.getMap().at("BestWorstPlayer")->getContentAsString();

		// Launch the asynchronous operation
		std::make_shared<Session>(ioContext)->run(host, port, bestWorstPlayer);
		break;
	}
	case 2:
	{
		requestsManager.emplacePlayersRankingRequest();
		auto playersRanking = requestsManager.getMap().at("PlayersRanking")->getContentAsString();

		std::make_shared<Session>(ioContext)->run(host, port, playersRanking);
		break;
	}
	case 3:
	{
		requestsManager.emplaceScoreRequest();
		auto scores = requestsManager.getMap().at("Scores")->getContentAsString();

		std::make_shared<Session>(ioContext)->run(host, port, scores);
		break;
	}
	case 4:
	{
		requestsManager.emplacePlayerAfterCriteriaRequest();
		auto playerAfterCriteria = requestsManager.getMap().at("PlayerAfterCriteria")->getContentAsString();


		std::make_shared<Session>(ioContext)->run(host, port, playerAfterCriteria);
		break;
	}
	}

	// Run the I/O service. The call will return when
	// the socket is closed.
	ioContext.run();

	return EXIT_SUCCESS;
}