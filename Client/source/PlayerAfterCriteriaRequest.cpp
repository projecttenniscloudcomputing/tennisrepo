#include "..\include\PlayerAfterCriteriaRequest.h"

#include<iostream>
PlayerAfterCriteriaRequest::PlayerAfterCriteriaRequest():Request("PlayerAfterCriteria")
{
	int choice;
	std::string request;

	std::cout << "Please choose one of the following options:" << std::endl;

	std::cout << " 1 - To show all players with the same first name." << std::endl;
	std::cout << " 2 - To show the players with the same second name." << std::endl;
	std::cout << " 3 - To show all players who have taken part to a certain tourney" << std::endl;

	std::cin >> choice;

	switch (choice)
	{
	case 1:
	{
		std::string firstName;

		std::cout << "Please insert the first name of a player." << std::endl;
		std::cin >> firstName;
		this->content.push_back(boost::property_tree::ptree::value_type("Choice", std::to_string(choice)));
		this->content.push_back(boost::property_tree::ptree::value_type("FirstName", firstName));
		break;
	}
	case 2:
	{
		std::string secondName;

		std::cout << "Please insert the second name of a player." << std::endl;
		std::cin >> secondName;
		this->content.push_back(boost::property_tree::ptree::value_type("Choice", std::to_string(choice)));
		this->content.push_back(boost::property_tree::ptree::value_type("SecondName", secondName));
		break;
	}
	case 3:
	{
		std::string tourneyName;

		std::cout << "Please insert the tourney's name to see the players who has attended to. "<<std::endl;
		std::cin >> tourneyName;
		this->content.push_back(boost::property_tree::ptree::value_type("Choice", std::to_string(choice)));
		this->content.push_back(boost::property_tree::ptree::value_type("TourneyName", tourneyName));
		break;
	}
	}


}
