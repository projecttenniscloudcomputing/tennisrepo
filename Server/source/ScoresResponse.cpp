#include "..\include\ScoresResponse.h"
#include"Utils.h"
ScoresResponse::ScoresResponse():Response("Scores")
{
}

std::string ScoresResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	Utils utils;
	std::stringstream ss;

	std::string response;

	auto choice = atoi(packet.get<std::string>("Choice").c_str());

	switch (choice)
	{
	case 1:
	{
	    int year = atoi(packet.get<std::string>("Year").c_str());
		std::vector<Match> matches = utils.getAllMatchesFromACertainYear(year);

		if (matches.size() == 0)
		{
			response = "No scores to diplay for this year.";
			break;
		}

		ss << "The scores from year " << year << " are ";
		for each (Match match in matches)
		{
			ss << "Winner: " << match.getWinnerPlayer().toString() << " Loser: " << match.getLoserPlayer().toString();
		}

		response = ss.str();
		break;
	}
	case 2:
	{
		std::string playerId = packet.get<std::string>("PlayerId").c_str();

		std::map<std::string, Player> players = utils.getPlayers();
		Player player = players[playerId];

		std::vector<Match> matches = utils.getAllMatchesFromACertainPlayer(player);
		if (matches.size() == 0)
		{
			response = "There is no score for this player.";
			break;
		}

		ss << "The scores for player " << player.toString() << " are ";
		for each (Match match in matches)
		{
			ss << "Winner: " << match.getWinnerPlayer().toString() << " Loser: " << match.getLoserPlayer().toString();
		}

		response = ss.str();

		break;
	}
	}

	this->content.push_back(boost::property_tree::ptree::value_type("File", "Scores.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", response));

	return this->getContentAsString();
}
