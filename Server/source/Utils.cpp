#include "..\include\Utils.h"

#include<fstream>
#include<iostream>
#include<utility>
#include<algorithm>

const int yearLength = 4;
const int topPlayersNumber = 10;

struct Comparator
{
	bool operator()(const Player & firstPlayer, const Player & secondPlayer)const { return firstPlayer > secondPlayer; }
};

Utils::Utils()
{
	InitPlayers();
	InitTourneys();
}

void Utils::InitPlayers()
{
	std::ifstream inputStream;
	// opening the stream
	inputStream.open("C:\\Users\\Lenovo\\Desktop\\files\\players.csv");

	if (!inputStream.is_open())
		std::cout << "Failed to open the file";

	// we don't need the first line of the document
	std::string dummyString;
	std::getline(inputStream, dummyString, '\n');


	while (inputStream.good())
	{
		// fields of excel file

		std::string playerId;
		std::string playerSlug;
		std::string firstName;
		std::string lastName;
		std::string playerUrl;
		std::string flagCode;
		std::string residence;
		std::string birthPlace;
		std::string birthDate;
		std::string birthYear;
		std::string birthMonth;
		std::string birthDay;
		std::string turnedPro;
		std::string weightLbs;
		std::string weightKg;
		std::string heightFeet;
		std::string heightInches;
		std::string heightCentimeters;
		std::string handedness;
		std::string backHand;


		// reading every field info from the file

		std::getline(inputStream, playerId, ',');
		std::getline(inputStream, playerSlug, ',');
		std::getline(inputStream, firstName, ',');
		std::getline(inputStream, lastName, ',');
		std::getline(inputStream, playerUrl, ',');
		std::getline(inputStream, flagCode, ',');

		if (inputStream.peek() == '"')
		{
			std::getline(inputStream, dummyString, '"');
			std::getline(inputStream, residence, '"');
			std::getline(inputStream, dummyString, ',');
		}
		else
		{
			std::getline(inputStream, residence, ',');
		}

		if (inputStream.peek() == '"')
		{

			std::getline(inputStream, dummyString, '"');
			std::getline(inputStream, birthPlace, '"');
			std::getline(inputStream, dummyString, ',');
		}

		else
		{
			std::getline(inputStream, birthPlace, ',');
		}

		std::getline(inputStream, birthDate, ',');
		std::getline(inputStream, birthYear, ',');
		std::getline(inputStream, birthMonth, ',');
		std::getline(inputStream, birthDay, ',');
		std::getline(inputStream, turnedPro, ',');
		std::getline(inputStream, weightLbs, ',');
		std::getline(inputStream, weightKg, ',');

		if (inputStream.peek() == '"')
		{
			std::getline(inputStream, dummyString, '"');
			std::getline(inputStream, heightFeet, '"');
			std::getline(inputStream, dummyString, ',');
		}

		else
		{
			std::getline(inputStream, heightFeet, ',');
		}


		std::getline(inputStream, heightInches, ',');
		std::getline(inputStream, heightCentimeters, ',');
		std::getline(inputStream, handedness, ',');
		std::getline(inputStream, backHand, '\n');

		// initialising a new player
		Player player;

		// setting player's fields
		player.setPlayerId(playerId);
		player.setPlayerSlug(playerSlug);
		player.setPlayerFirstName(firstName);
		player.setPlayerLastName(lastName);
		player.setPlayerURL(playerUrl);
		player.setPlayerFlagCode(flagCode);
		player.setResidence(residence);
		player.setBirthPlace(birthPlace);
		player.setBirthDate(birthDate);

		if (birthYear != "") player.setBirthYear(std::stoi(birthYear));
		if (birthMonth != "") player.setBirthMonth(std::stoi(birthMonth));
		if (birthDay != "") player.setBirthDay(std::stoi(birthDay));
		if (turnedPro != "") player.setTurnedPro(std::stoi(turnedPro));
		if (weightLbs != "") player.setWeightLbs(std::stoi(weightLbs));
		if (weightKg != "")player.setWeightKg(std::stoi(weightKg));
		player.setHeightFeet(heightFeet);
		if (heightInches != "") player.setHeightInches(std::stoi(heightInches));
		if (heightCentimeters != "")player.setHeightCentimeters(std::stoi(heightCentimeters));
		player.setHandedness(handedness);
		player.setBackhand(backHand);

		// then we add it to the list
		

		m_players.insert(std::pair<std::string, Player>(playerId, player));
	}

	// closing the stream
	inputStream.close();
}

void Utils::InitTourneys()
{
	std::ifstream inputStream;
	// opening the stream
	inputStream.open("C:\\Users\\Lenovo\\Desktop\\files\\match_scores.csv");

	if (!inputStream.is_open())
		std::cout << "Failed to open the file";

	// we don't need the first line of the document
	std::string dummyString;
	std::getline(inputStream, dummyString, '\n');

	// string which ckecks if the tourney id has changed
	std::string tourneyChecker;

	while (inputStream.good())
	{
		std::string tourneyYearId;

		// fields of excel file

		std::string tourneyYear;
		std::string tourneyOrder;
		std::string tourneySlug;
		std::string tourneyUrlSuffix;
		std::string tourneyRoundName;
		std::string roundOrder;
		std::string matchOrder;
		std::string winnerName;
		std::string winnerPlayerId;
		std::string winnerPlayerSlug;
		std::string loserName;
		std::string loserPlayerId;
		std::string loserSlug;
		std::string winnerSeed;
		std::string loserSeed;
		std::string matchScoreTiebreaks;
		std::string winnerSetsWon;
		std::string loserSetsWon;
		std::string winnerGamesWon;
		std::string loserGamesWon;
		std::string winnerTiebreaksWon;
		std::string loserTiebreaksWon;
		std::string matchId;


		// reading every field's info from the file

		std::getline(inputStream, tourneyYearId, ',');

		if (tourneyYearId.size() > 3)
		{
			for (int index = 0; index < yearLength; ++index)
			{
				tourneyYear += tourneyYearId[index];
			}
		}

		std::getline(inputStream, tourneyOrder, ',');
		std::getline(inputStream, tourneySlug, ',');
		std::getline(inputStream, tourneyUrlSuffix, ',');
		std::getline(inputStream, tourneyRoundName, ',');
		std::getline(inputStream, roundOrder, ',');
		std::getline(inputStream, matchOrder, ',');
		std::getline(inputStream, winnerName, ',');
		std::getline(inputStream, winnerPlayerId, ',');
		std::getline(inputStream, winnerPlayerSlug, ',');
		std::getline(inputStream, loserName, ',');
		std::getline(inputStream, loserPlayerId, ',');
		std::getline(inputStream, loserSlug, ',');
		std::getline(inputStream, winnerSeed, ',');
		std::getline(inputStream, loserSeed, ',');
		std::getline(inputStream, matchScoreTiebreaks, ',');
		std::getline(inputStream, winnerSetsWon, ',');
		std::getline(inputStream, loserSetsWon, ',');
		std::getline(inputStream, winnerGamesWon, ',');
		std::getline(inputStream, loserGamesWon, ',');
		std::getline(inputStream, winnerTiebreaksWon, ',');
		std::getline(inputStream, loserTiebreaksWon, ',');
		std::getline(inputStream, matchId, '\n');

		if (tourneyYearId != tourneyChecker)
		{
			tourneyChecker = tourneyYearId;
			Tour tour;
			// tourney's information
			tour.setTourName(tourneySlug);
			if (tourneyYear != "") tour.setTourYear(std::stoi(tourneyYear));
			if (tourneyOrder != "") tour.setRoundOrder(std::stoi(tourneyOrder));
			tour.setTourUrlSuffix(tourneyUrlSuffix);
			tour.setTourRoundName(tourneyRoundName);
			if (roundOrder != "") tour.setRoundOrder(std::stoi(roundOrder));


			// all matches from the current tourney

			// creating a new match

			Match match;
			if (matchId != "")match.setMatchId(std::stoi(matchId));
			if (matchOrder != "")match.setMatchOrder(std::stoi(matchOrder));


			// in case of winner player we add a win to him
			m_players[winnerPlayerId].addWin();
			Player winnerPlayer = m_players[winnerPlayerId];
			match.setWinnerPlayer(winnerPlayer);

			// in case of loser player we add a loss to him
			m_players[loserPlayerId].addLose();
			Player loserPlayer = m_players[loserPlayerId];
			match.setLoserPlayer(loserPlayer);

			// adding the players to players's sets
			tour.addPlayer(winnerPlayer);
			tour.addPlayer(loserPlayer);

			// now we set the informations regarding to match
			match.setWinnerSeed(winnerSeed);
			match.setLoserSeed(loserSeed);
			if (winnerSetsWon != "") match.setWinnerSetsWon(std::stoi(winnerSetsWon));
			if (loserSetsWon != "") match.setLoserSetsWon(std::stoi(loserSetsWon));
			if (winnerGamesWon != "")match.setWinnerGamesWon(std::stoi(winnerGamesWon));
			if (loserGamesWon != "")match.setLoserGamesWon(std::stoi(loserGamesWon));
			if (winnerTiebreaksWon != "")match.setWinnerTiebreaksWon(std::stoi(winnerTiebreaksWon));
			if (loserTiebreaksWon != "")match.setLoserTiebreaksWon(std::stoi(loserTiebreaksWon));

			match.setScoreTiebreaks(matchScoreTiebreaks);

			// adding the match to the tourney

			tour.addMatch(match);
			// adding the match into tourneys list
			m_tourneys.push_back(tour);
		}
		else
		{
			// we have the same tourney so in this case we get the last tourney from list, we update it and put it back
			int size = m_tourneys.size();
			Tour tour = m_tourneys[size - 1];
			m_tourneys.pop_back();

			// creating a new match

			Match match;
			match.setMatchId(std::stoi(matchId));
			match.setMatchOrder(std::stoi(matchOrder));

			// in case of winner player we add a win to him
			m_players[winnerPlayerId].addWin();
			Player winnerPlayer = m_players[winnerPlayerId];
			match.setWinnerPlayer(winnerPlayer);

			// in case of loser player we add a loss to him
			m_players[loserPlayerId].addLose();
			Player loserPlayer = m_players[loserPlayerId];
			match.setLoserPlayer(loserPlayer);

			// adding the players to players's sets
			tour.addPlayer(winnerPlayer);
			tour.addPlayer(loserPlayer);


			// now we set the informations regarding to match
			match.setWinnerSeed(winnerSeed);
			match.setLoserSeed(loserSeed);
			match.setWinnerSetsWon(std::stoi(winnerSetsWon));
			match.setLoserSetsWon(std::stoi(loserSetsWon));
			match.setWinnerGamesWon(std::stoi(winnerGamesWon));
			match.setLoserGamesWon(std::stoi(loserGamesWon));
			match.setWinnerTiebreaksWon(std::stoi(winnerTiebreaksWon));
			match.setLoserTiebreaksWon(std::stoi(loserTiebreaksWon));

			match.setScoreTiebreaks(matchScoreTiebreaks);

			// adding a new match to the tourney

			tour.addMatch(match);
			m_tourneys.push_back(tour);
		}

		
	}

	// closing the stream
	inputStream.close();
}

std::map<std::string, Player> Utils::getPlayers() const
{
	return m_players;
}

std::vector<Tour> Utils::getTourneys() const
{
	return m_tourneys;
}

// FUNCTIONS NEEDED FOR BEST/WORST PLAYER CLASS

// function to get players with the most loss
std::vector<Player> Utils::getPlayersWithTheMostLoss() const
{
	std::vector<Player> players;
	int maximumNumberOfLoss = 0;

	for (auto pair : m_players)
	{
		int numberOfLoss = pair.second.getLoss();
		if (numberOfLoss > maximumNumberOfLoss)
		{
			maximumNumberOfLoss = numberOfLoss;
		}

	}

	for (auto pair : m_players)
	{
		Player player = pair.second;
		if (player.getLoss() == maximumNumberOfLoss)
		{
			players.push_back(player);
		}
	}

	return players;
}

// function to get players with the most wins
std::vector<Player> Utils::getPlayersWithTheMostWins() const
{
	std::vector<Player> players;
	int maximumNumberOfWins = 0;

	for (auto pair : m_players)
	{
		int numberOfWins = pair.second.getWins();
		if (numberOfWins > maximumNumberOfWins)
		{
			maximumNumberOfWins = numberOfWins;
		}

	}

	for (auto pair : m_players)
	{
		Player player = pair.second;
		if (player.getWins() == maximumNumberOfWins)
		{
			players.push_back(player);
		}
	}

	return players;
}

// FUNCTIONS NEEDED FOR PLAYERS RANKING

std::vector<Player> Utils::getTopPlayersOfACertainNationality(std::string & nationality) const
{
	std::vector<Player> players;
	for each (auto pair in m_players)
	{
		Player player = pair.second;
		if (player.getPlayerFlagCode() == nationality)
		{
			players.push_back(player);
		}
	}

	if (players.size() == 0) return players;

	// get the top players
	std::vector<Player> topPlayers;

	std::sort(players.begin(), players.end(), Comparator());

	int size;
	if (topPlayersNumber > players.size())
	{
		size = players.size();
	}
	else
	{
		size = topPlayersNumber;
	}

	for (int index = 0; index < size; ++index)
	{
		Player player = players[index];
		topPlayers.push_back(player);
	}

	return topPlayers;
}

std::vector<Player> Utils::getTopPlayersOfACertainYear(int year) const
{
	std::vector<Player> players;
	for each (Tour tour in m_tourneys)
	{
		if (tour.getTourYear() == year)
		{
			for each (Player player in tour.getPlayers())
			{
				players.push_back(player);
			}
		}
	}


	if (players.size() == 0) return players;

	// get the top players
	std::vector<Player> topPlayers;

	std::sort(players.begin(), players.end(), Comparator());

	int size;
	if (topPlayersNumber > players.size())
	{
		size = players.size();
	}
	else
	{
		size = topPlayersNumber;
	}
	for (int index = 0; index < size; ++index)
	{
		Player player = players[index];
		topPlayers.push_back(player);
	}

	return topPlayers;
}

// FUNCTIONS NEEDED FOR SCORES

std::vector<Match> Utils::getAllMatchesFromACertainYear(int year) const
{
	std::vector<Match> matches;

	for each (Tour tour in m_tourneys)
	{
		if (tour.getTourYear() == year)
		{
			for each (Match match  in tour.getMatches())
			{
				matches.push_back(match);
			}
		}
	}

	return matches;
}

std::vector<Match> Utils::getAllMatchesFromACertainPlayer(Player & player) const
{
	std::vector<Match> matches;

	for each (Tour tour in m_tourneys)
	{
		for each (Match match in tour.getMatches())
		{
			if (match.getWinnerPlayer() == player || match.getLoserPlayer() == player)
			{
				matches.push_back(match);
			}
		}
	}

	return matches;
}


// FUNCTIONS NEEDED TO GET PLAYERS WITH A CERTAIN CRITERIA

std::vector<Player> Utils::getAllPlayersWithFirstName(std::string & firstName) const
{
	std::vector<Player> players;

	for each (auto pair in m_players)
	{
		if (pair.second.getPlayerFirstName() == firstName)
		{
			players.push_back(pair.second);
		}
	}

	return players;
}

std::vector<Player> Utils::getAllPlayersWithSecondName(std::string & secondName) const
{
	std::vector<Player> players;

	for each (auto pair in m_players)
	{
		if (pair.second.getPlayerLastName() == secondName)
		{
			players.push_back(pair.second);
		}
	}

	return players;
}

Tour Utils::getAllPlayersFromTourneyWithName(std::string & tourName) const
{
	Tour emptyTour;

	for each (Tour tour in m_tourneys)
	{
		if (tour.getTourName() == tourName) return tour;
	}

	return emptyTour;
}




