#include "..\include\PlayerAfterCriteriaResponse.h"
#include"Utils.h"


PlayerAfterCriteriaResponse::PlayerAfterCriteriaResponse():Response("PlayerAfterCriteria")
{
}

std::string PlayerAfterCriteriaResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	Utils utils;
	std::stringstream ss;

	std::string response;

	auto choice = atoi(packet.get<std::string>("Choice").c_str());

	switch (choice)
	{
	case 1:
	{
		std::string firstName = packet.get<std::string>("FirstName").c_str();
		std::vector<Player> players = utils.getAllPlayersWithFirstName(firstName);

		if (players.size() == 0)
		{
			response = "There is no player with this first name !";
			break;
		}

		for each (Player player in players)
		{
			ss << player.toString();
		}
		break;
	}
	case 2:
	{
		std::string secondName = packet.get<std::string>("SecondName").c_str();
		std::vector<Player> players = utils.getAllPlayersWithSecondName(secondName);

		if (players.size() == 0)
		{
			response = "There is no player with this second name !";
			break;
		}

		for each (Player player in players)
		{
			ss << player.toString();
		}
		break;
	}
	case 3:
	{
		std::string tourName = packet.get<std::string>("TourneyName").c_str();
		Tour tour = utils.getAllPlayersFromTourneyWithName(tourName);

		if (tour.getTourName()=="")
		{
			response = "There is no tourney with this name !";
			break;
		}
		ss << "The players from tourney " << tourName << " are :";
		for each (Player player in tour.getPlayers())
		{
			ss << player.toString();
		}
		break;
	}
	}

	this->content.push_back(boost::property_tree::ptree::value_type("File", "PlayerAfterCriteria.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", response));

	return this->getContentAsString();
}
