#include<sstream>

#include "Match.h"

Match::Match(int matchId, int matchOrder, Player winnerPlayer, Player loserPlayer):
	m_matchId(matchId),
	m_matchOrder(matchOrder),
	m_winnerPlayer(winnerPlayer),
	m_loserPlayer(loserPlayer)
{}

// GETTERS

int Match::getMatchId() const
{
	return m_matchId;
}

int Match::getMatchOrder() const
{
	return m_matchOrder;
}

Player Match::getWinnerPlayer() const
{
	return m_winnerPlayer;
}

Player Match::getLoserPlayer() const
{
	return m_loserPlayer;
}

std::string Match::getWinnerSeed() const
{
	return m_winnerSeed;
}

std::string Match::getLoserSeed() const
{
	return m_loserSeed;
}

std::string Match::getScoreTiebreaks() const
{
	return m_scoreTiebreaks;
}

int Match::getWinnerSetsWon()const
{
	return m_winnerSetsWon;
}

int Match::getLoserSetsWon()const
{
	return m_loserSetsWon;
}

int Match::getWinnerGamesWon()const
{
	return m_winnerGamesWon;
}

int Match::getLoserGamesWon()const
{
	return m_loserGamesWon;
}

int Match::getWinnerTiebreaksWon()const
{
	return m_winnerTiebreaksWon;
}

int Match::getLoserTiebreaksWon()const
{
	return m_loserTiebreaksWon;
}


// SETTERS

void Match::setMatchId(int id)
{
	m_matchId = id;
}

void Match::setMatchOrder(int order)
{
	m_matchOrder = order;
}

void Match::setWinnerPlayer(Player & winnerPlayer)
{
	m_winnerPlayer = winnerPlayer;
}

void Match::setLoserPlayer(Player & loserPlayer)
{
	m_loserPlayer = loserPlayer;
}

void Match::setWinnerSeed(std::string & winnerSeed)
{
	m_winnerSeed = winnerSeed;
}

void Match::setLoserSeed(std::string & loserSeed)
{
	m_loserSeed = loserSeed;
}

void Match::setScoreTiebreaks(std::string & scoreTieBreaks)
{
	m_scoreTiebreaks = scoreTieBreaks;
}

void Match::setWinnerSetsWon(int winnerSetsWon)
{
	m_winnerSetsWon = winnerSetsWon;
}

void Match::setLoserSetsWon(int loserSetsWon)
{
	m_loserSetsWon = loserSetsWon;
}

void Match::setWinnerGamesWon(int winnerGamesWon)
{
	m_winnerGamesWon = winnerGamesWon;
}

void Match::setLoserGamesWon(int loserGamesWon)
{
	m_loserGamesWon = loserGamesWon;
}

void Match::setWinnerTiebreaksWon(int winnerTiebreaksWon)
{
	m_winnerTiebreaksWon = winnerTiebreaksWon;
}

void Match::setLoserTiebreaksWon(int loserTiebreaksWon)
{
	m_loserTiebreaksWon = loserTiebreaksWon;
}

std::string Match::toString() const
{
	std::stringstream ss;

	ss << "Match Id:" << m_matchId << " Match order: " << m_matchOrder << "\nWinner player : "<<m_winnerPlayer.toString()<< "\nLoser player "<<m_loserPlayer.toString()<<"\n";
	return ss.str();
}

