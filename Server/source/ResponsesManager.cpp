#include "ResponsesManager.h"

#include "BestWorstPlayerResponse.h"
#include "PlayersRankingResponse.h"
#include"ScoresResponse.h"
#include"PlayerAfterCriteriaResponse.h"

ResponsesManager::ResponsesManager()
{
	this->Responses.emplace("BestWorstPlayer", std::make_shared<BestWorstPlayerResponse>());
	this->Responses.emplace("PlayersRanking", std::make_shared<PlayersRankingResponse>());
	this->Responses.emplace("Scores", std::make_shared<ScoresResponse>());
	this->Responses.emplace("PlayerAfterCriteria", std::make_shared<ScoresResponse>());
}

std::map<std::string, std::shared_ptr<Framework::Response>> ResponsesManager::getMap() const
{
	return this->Responses;
}
