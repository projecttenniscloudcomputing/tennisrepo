#include "..\include\BestWorstPlayerResponse.h"
#include"Utils.h"
#include<iostream>
BestWorstPlayerResponse::BestWorstPlayerResponse() :Response("BestWorstPlayer")
{
}

std::string BestWorstPlayerResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto request = atoi(packet.get<std::string>("Request").c_str());

	Utils utils;
	std::stringstream ss;

	std::string response;

	switch (request)
	{
	case 1:
	{
		std::vector<Player> players = utils.getPlayersWithTheMostWins();
		for each (Player player in players)
		{
			ss << player.toString();
		}
		response = ss.str();
		

		break;
	}
	case 2:
	{
		std::vector<Player> players = utils.getPlayersWithTheMostLoss();
		for each (Player player in players)
		{
			ss << player.toString();
		}
		response = ss.str();
		

		break;
	}

	}

	this->content.push_back(boost::property_tree::ptree::value_type("File", "BestWorst.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", response));

	return this->getContentAsString();
}
