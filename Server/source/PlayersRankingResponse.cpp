#include "..\include\PlayersRankingResponse.h"
#include"Utils.h"

PlayersRankingResponse::PlayersRankingResponse():Response("PlayersRanking")
{
}

std::string PlayersRankingResponse::interpretPacket(const boost::property_tree::ptree & packet)
{

	Utils utils;
	std::stringstream ss;

	std::string response;

	auto choice = atoi(packet.get<std::string>("Choice").c_str());

	switch (choice)
	{
	case 1:
	{
		std::string flagCode = packet.get<std::string>("FlagCode").c_str();
		std::vector<Player> topPlayers = utils.getTopPlayersOfACertainNationality(flagCode);

		if (topPlayers.size() == 0)
		{
			response = "The flag code you have introduced is not valid !";
			break;
		}

		for each (Player player in topPlayers)
		{
			ss << player.toString();
		}

		response = ss.str();
		break;
	}
	case 2:
	{
		int year= atoi(packet.get<std::string>("Year").c_str());
		std::vector<Player> topPlayers = utils.getTopPlayersOfACertainYear(year);

		if (topPlayers.size() == 0)
		{
			response = "There was no match in the year you have introduced !";
			break;
		}

		for each (Player player in topPlayers)
		{
			ss << player.toString();
		}

		response = ss.str();
		if (topPlayers.size() == 0) response = "There was no match in the year you have introduced !";

		break;
	}
	}

	this->content.push_back(boost::property_tree::ptree::value_type("File", "Ranking.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", response));

	return this->getContentAsString();
}
