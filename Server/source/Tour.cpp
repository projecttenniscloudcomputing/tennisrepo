#include<sstream>

#include "Tour.h"

Tour::Tour(int tourId, int tourYear, std::string tourName, std::string tourRoundName, std::vector<Match> matches)
{}

// getters
int Tour::getTourId() const
{
	return m_tourId;
}

int Tour::getTourYear() const
{
	return m_tourYear;
}

std::string Tour::getTourName() const
{
	return m_tourName;
}

std::string Tour::getTourRoundName() const
{
	return m_tourRoundName;
}

std::vector<Match> Tour::getMatches() const
{
	return m_matches;
}

std::set<Player> Tour::getPlayers() const
{
	return m_players;
}

int Tour::getRoundOrder() const
{
	return m_roundOrder;
}

std::string Tour::getTourUrlSuffix() const
{
	return m_tourUrlSuffix;
}


// setters

void Tour::setTourId(int tourId)
{
	m_tourId = tourId;
}


void Tour::setTourYear(int tourYear)
{
	m_tourYear = tourYear;
}

void Tour::setTourName(std::string & tourName)
{
	m_tourName = tourName;
}

void Tour::setTourRoundName(std::string & tourRoundName)
{
	m_tourRoundName = tourRoundName;
}

void Tour::setRoundOrder(int roundOrder)
{
	m_roundOrder=roundOrder;
}

void Tour::setTourUrlSuffix(std::string &)
{
	m_tourUrlSuffix;
}

void Tour::addMatch(Match & match)
{
	m_matches.push_back(match);
}

void Tour::addPlayer(Player & player)
{
	m_players.insert(player);
}

std::string Tour::toString()
{
	std::stringstream ss;

	ss << "Tour Id : " << m_tourId << " Tour name : " << m_tourName;
	ss << "\nThe matches from this tourney are:\n";
	for each ( Match match in m_matches)
	{
		ss << match.toString() << "\n";
	}
	return ss.str();

}
