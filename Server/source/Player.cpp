#include<sstream>

#include "Player.h"

// GETTERS

Player::Player()
{
	m_wins = 0;
	m_loss = 0;
}

std::string Player::getPlayerId() const
{
	return m_playerId;
}

std::string Player::getPlayerSlug() const
{
	return m_playerSlug;
}

std::string Player::getPlayerFirstName() const
{
	return m_firstName;
}

std::string Player::getPlayerLastName() const
{
	return m_lastName;
}

std::string Player::getPlayerURL() const
{
	return m_playerUrl;
}

std::string Player::getPlayerFlagCode() const
{
	return m_flagCode;
}

std::string Player::getPlayerResidence() const
{
	return m_residence;
}

std::string Player::getPlayerBirthplace() const
{
	return m_birthplace;
}

std::string Player::getPlayerBirthdate() const
{
	return m_birthdate;
}

int Player::getBirthYear() const
{
	return m_birthYear;
}

int Player::getBirthMonth() const
{
	return m_birthMonth;
}

int Player::getBirthDay() const
{
	return m_birthDay;
}

int Player::getTurnedPro() const
{
	return m_turnedPro;
}

int Player::getWeightLbs() const
{
	return m_weightInLbs;
}

int Player::getWeightKg() const
{
	return m_weightInKg;
}

std::string Player::getHeightFeet() const
{
	return m_heightInFeets;
}

int Player::getHeightInches() const
{
	return m_heightInInches;
}

int Player::getHeightCentimeters() const
{
	return m_heightInCentimeters;
}

std::string Player::getHandedness() const
{
	return m_handedness;
}

std::string Player::getBackHand() const
{
	return m_backHand;
}

int Player::getWins() const
{
	return m_wins;
}

int Player::getLoss() const
{
	return m_loss;
}

int Player::getRankPosition() const
{
	return m_rankPostion;
}

// SETTERS

void Player::setPlayerId(std::string & id)
{
	m_playerId = id;
}

void Player::setPlayerSlug(std::string & slug)
{
	m_playerSlug = slug;
}

void Player::setPlayerFirstName(std::string & firstName)
{
	m_firstName = firstName;
}

void Player::setPlayerLastName(std::string & lastName)
{
	m_lastName = lastName;
}

void Player::setPlayerFlagCode(std::string & flagCode)
{
	m_flagCode = flagCode;
}

void Player::setPlayerURL(std::string & playerUrl)
{
	m_playerUrl = playerUrl;
}

void Player::setResidence(std::string & residence)
{
	m_residence = residence;
}

void Player::setBirthPlace(std::string & birthPlace)
{
	m_birthplace = birthPlace;
}

void Player::setBirthDate(std::string & birthDate)
{
	m_birthdate = birthDate;
}

void Player::setBirthYear(int  birthYear)
{
	m_birthYear = birthYear;
}

void Player::setBirthMonth(int birthMonth)
{
	m_birthMonth = birthMonth;
}

void Player::setBirthDay(int  birthDay)
{
	m_birthDay = birthDay;
}

void Player::setTurnedPro(int  turnedPro)
{
	m_turnedPro = turnedPro;
}

void Player::setWeightLbs(int  weightLbs)
{
	m_weightInLbs = weightLbs;
}

void Player::setWeightKg(int  weightKg)
{
	m_weightInKg = weightKg;
}

void Player::setHeightFeet(std::string & heightFeet)
{
	m_heightInFeets = heightFeet;
}

void Player::setHeightInches(int  heightInches)
{
	m_heightInInches = heightInches;
}

void Player::setHeightCentimeters(int  heightCentimeters)
{
	m_heightInCentimeters = heightCentimeters;
}

void Player::setHandedness(std::string & handedness)
{
	m_handedness = handedness;
}

void Player::setBackhand(std::string & backHand)
{
	m_backHand = backHand;
}


// FUNCTIONS TO SET RANK , ADD WIN OR LOSE TO PLAYER

void Player::setRankPosition(int & position)
{
	m_rankPostion = position;
}

void Player::addWin()
{
	++m_wins;
}

void Player::addLose()
{
	++m_loss;
}

// function that returns a player as a string
std::string Player::toString() const
{
	std::stringstream ss;

	ss << "Id: " << m_playerId << " First Name: " << m_firstName << " LastName: " << m_lastName << " Player Slug: " << m_playerSlug << " Nationality: " << m_flagCode
		<< " Wins: " << m_wins << " Loss: " << m_loss;;

	return ss.str();
}

bool Player::operator>(const Player & otherPlayer) const
{
	return m_wins > otherPlayer.m_wins;
}

bool Player::operator<(const Player & otherPlayer) const
{
	return m_wins < otherPlayer.m_wins;
}

bool Player::operator==(const Player & otherPlayer) const
{
	return m_playerId == otherPlayer.m_playerId;
}





