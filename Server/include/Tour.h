#pragma once
#include<vector>
#include<set>

#include"Match.h"
class Tour
{
public:
	Tour() = default;
	Tour(int,int,std::string,std::string, std::vector<Match>);
	~Tour() = default;

	// getters
	int getTourId()const;
	int getTourYear()const;
	std::string getTourName()const;
	std::string getTourRoundName()const;
	std::vector<Match> getMatches()const;
	std::set<Player>getPlayers()const;
	int getRoundOrder()const;
	std::string getTourUrlSuffix()const;

	// setters
	void setTourId(int );
	void setTourYear(int);
	void setTourName(std::string &);
	void setTourRoundName(std::string &);
	void setRoundOrder(int);
	void setTourUrlSuffix(std::string &);


	// function for adding a new match
	void addMatch(Match &);

	// function to add a new Player

	void addPlayer(Player &);

	// to string function to print tournaments content

	std::string toString();

private:

	int m_tourId;
	int m_tourYear;
	std::string m_tourName;
	std::string m_tourRoundName;
	std::string m_tourUrlSuffix;
	int m_roundOrder;

	// matches
	std::vector<Match> m_matches;
	std::set<Player> m_players;
};

