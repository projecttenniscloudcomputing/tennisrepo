#pragma once

#include"Response.h"

class PlayersRankingResponse :public Framework::Response
{
public:
	PlayersRankingResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};