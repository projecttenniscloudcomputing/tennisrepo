#pragma once

#include "Response.h"

class BestWorstPlayerResponse :public Framework::Response
{
public:
	BestWorstPlayerResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};