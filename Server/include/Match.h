#pragma once
#include<vector>

#include"Player.h"

class Match
{
public:
	Match() = default;
	Match(int, int, Player, Player);
	~Match() = default;

	// getters
	int getMatchId()const;
	int getMatchOrder()const;
	Player getWinnerPlayer()const;
	Player getLoserPlayer()const;
	std::string getWinnerSeed()const;
	std::string getLoserSeed()const;
	std::string getScoreTiebreaks()const;
	int getWinnerSetsWon()const;
	int getLoserSetsWon()const;
	int getWinnerGamesWon()const;
	int getLoserGamesWon()const;
	int getWinnerTiebreaksWon()const;
	int getLoserTiebreaksWon()const;


	// setters
	void setMatchId(int);
	void setMatchOrder(int );
	void setWinnerPlayer(Player &);
	void setLoserPlayer(Player &);
	void setWinnerSeed(std::string &);
	void setLoserSeed(std::string &);
	void setScoreTiebreaks(std::string &);
	void setWinnerSetsWon(int);
	void setLoserSetsWon(int);
	void setWinnerGamesWon(int);
	void setLoserGamesWon(int);
	void setWinnerTiebreaksWon(int);
	void setLoserTiebreaksWon(int);

	// printing function

	std::string toString()const;

private:
	int m_matchId;
	int m_matchOrder;
	Player m_winnerPlayer;
	Player m_loserPlayer;

	std::string m_winnerSeed;
	std::string m_loserSeed;
	std::string m_scoreTiebreaks;
	int m_winnerSetsWon;
	int m_loserSetsWon;
	int m_winnerGamesWon;
	int m_loserGamesWon;
	int m_winnerTiebreaksWon;
	int m_loserTiebreaksWon;
};

