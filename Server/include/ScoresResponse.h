#pragma once

#include"Response.h"

class ScoresResponse :public Framework::Response
{
public:
	ScoresResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};