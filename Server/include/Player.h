#pragma once

#include<string>

class Player
{
public:
	Player();
	~Player() = default;

	// getters
	std::string getPlayerId()const;
	std::string getPlayerSlug()const;
	std::string getPlayerFirstName()const;
	std::string getPlayerLastName()const;
	std::string getPlayerURL()const;
	std::string getPlayerFlagCode()const;
	std::string getPlayerResidence()const;
	std::string getPlayerBirthplace()const;
	std::string getPlayerBirthdate()const;
	int getBirthYear()const;
	int getBirthMonth()const;
	int getBirthDay()const;
	int getTurnedPro()const;
	int getWeightLbs()const;
	int getWeightKg()const;
	std::string getHeightFeet()const;
	int getHeightInches()const;
	int getHeightCentimeters()const;
	std::string getHandedness()const;
	std::string getBackHand()const;




	int getWins()const;
	int getLoss()const;
	int getRankPosition()const;

	//setters
	void setPlayerId(std::string &);
	void setPlayerSlug(std::string &);
	void setPlayerFirstName(std::string &);
	void setPlayerLastName(std::string &);
	void setPlayerFlagCode(std::string &);
	void setPlayerURL(std::string &);
	void setResidence(std::string &);
	void setBirthPlace(std::string &);
	void setBirthDate(std::string &);
	void setBirthYear(int);
	void setBirthMonth(int);
	void setBirthDay(int);
	void setTurnedPro(int);
	void setWeightLbs(int);
	void setWeightKg(int);
	void setHeightFeet(std::string &);
	void setHeightInches(int);
	void setHeightCentimeters(int);
	void setHandedness(std::string &);
	void setBackhand(std::string &);


	void setRankPosition(int &);

	// function to add a win to the player
	void addWin();

	// function to add a lose to the player

	void addLose();

	// function to get the player as a string

	std::string toString()const;

	// comparators
	bool operator > (const Player & )const;
	bool operator < (const Player &)const;
	bool operator==(const Player &)const;

private:
	// columns from players.csv

	std::string m_playerId;
	std::string m_playerSlug;
	std::string m_firstName;
	std::string m_lastName;
	std::string m_playerUrl;
	std::string m_flagCode;
	std::string m_residence;
	std::string m_birthplace;
	std::string m_birthdate;
	int m_birthYear;
	int m_birthMonth;
	int m_birthDay;
	int m_turnedPro;
	int m_weightInLbs;
	int m_weightInKg;
	std::string m_heightInFeets;
	int m_heightInInches;
	int m_heightInCentimeters;
	std::string m_handedness;
	std::string m_backHand;

	// fields calculated
	int m_wins;
	int m_loss;
	int m_rankPostion;

};

