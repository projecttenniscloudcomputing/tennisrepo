#pragma once

#include"Tour.h";
#include<map>

class Utils
{
public:
	Utils();
	~Utils() = default;
 
	void InitPlayers();
	void InitTourneys();

	// getters  

	std::map<std::string, Player> getPlayers()const;
	std::vector<Tour> getTourneys()const;

	// functions

	std::vector<Player> getPlayersWithTheMostLoss()const;
	std::vector<Player> getPlayersWithTheMostWins()const;

	std::vector<Player> getTopPlayersOfACertainNationality(std::string &)const;
	std::vector<Player> getTopPlayersOfACertainYear(int)const;

	std::vector<Match> getAllMatchesFromACertainYear(int)const;
	std::vector<Match> getAllMatchesFromACertainPlayer(Player &)const;

	std::vector<Player> getAllPlayersWithFirstName(std::string &)const;
	std::vector<Player> getAllPlayersWithSecondName(std::string &)const;
	Tour getAllPlayersFromTourneyWithName(std::string &)const;

private:
	
	std::map<std::string, Player> m_players;
	std::vector<Tour> m_tourneys;
	
};