#pragma once

#include"Response.h"

class PlayerAfterCriteriaResponse :public Framework::Response
{
public:
	PlayerAfterCriteriaResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};